There are some important areas you can contribute to this project:

## Code
* Have better visuals during checking connection.
* Check bridges syntax based on bridge type.
* Port About window to Libadwaita 1.3.
* Move SaveButton click logic to actions.

## Community
* Improvement in Pypi release to support desktop-related issues better.
* Making native package build recepies for more GNU/Linux distros.
* Designing a better app icon to suit other GNOME apps.
* Making [appstream](https://www.freedesktop.org/software/appstream/docs/) data correct.
* Releasing on Flathub and moving it to GNOME Circle.
