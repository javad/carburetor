��    ?        Y         p     q     �  *   �     �     �     �     �     �     �  
             (     6     <     E     V  	   c     m     u     �     �     �     �  	   �     �  +   �  1   �  ;   !     ]     e     q  "   x     �     �     �     �     �     �     �  ,   �  
   �  	     
             )     /     7     >  $   J     o     �     �     �     �     �     �     �     �     	     	     	     	  �  /	     �
     �
  A     
   E     P     \     b     k     �  
   �     �     �     �     �     �     �     �     �       	             5     >  	   G     Q  !   X  !   z  +   �     �  	   �     �     �     �                         &     .  (   7     `  	   l  
   v  	   �     �     �     �     �  "   �     �     �     �     �               +     C  
   U  
   `     k     r  (   y                  $   )   3   -      ?       (       2          :                                          5   .      ,   9   	   <       #   +       *            &             %          0          '      /             ;   "                  8         6       7       4      =       
       1         >      !                    About Carburetor Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Client Transport Plugin Connecting… Czech DNS Port Disconnecting… Exit Country Exit Node Finland For Tractor France GTK frontend for Tractor General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands Norway Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Save Bridges as a file in your local configs Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor is not running! Tractor is stopped Type of bridge Ukraine United Kingdom United States You have a new identity! _Check connection _Connect _Disconnect _New ID _Save translator-credits Project-Id-Version: Norwegian Bokmål (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Norwegian Bokmål <https://hosted.weblate.org/projects/carburetor/translations/nb_NO/>
Language: nb_NO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.15-dev
 Om Carburetor Godta tilkobling Hvorvidt eksterne enheter skal tillates å bruke dette nettverket Østerrike Auto (best) Broer Bulgaria Kan ikke lagre broene Canada Carburetor Klienttransport-programtillegg Kobler til … Tsjekkia DNS-port Kobler fra … Avslutt land Utgangsnode Finland For Tractor Frankrike GTK-skjermflate for Tractor Generelt Tyskland HTTP-port Irland Lokal port Tractor skal lytte til Lokal port Tractor skal lytte til Lokal port du har en anonym navnetjener på Moldova Nederland Norge Sjekk syntaksen for broene Polen Porter Innstillinger Avslutt Romania Kjører Russland Lagre broer i en fil i ditt lokaloppsett Seychellene Singapore SOCKS-port Kildekode Spania Stoppet Sverige Sveits Landet du ønsker å koble til fra Tractor kjører ikke! Tractor er stoppet Brotype Ukraina Storbritannia Amerikas forente stater Du har en ny identitet! _Sjekk tilkobling _Koble til _Koble fra _Ny ID _Lagre Allan Nordhøy, <epost@anotheragency.no> 