��    ?        Y         p     q     �  *   �     �     �     �     �     �     �  
             (     6     <     E     V  	   c     m     u     �     �     �  	   �     �  +   �  1   �  ;        D     L     X  "   _     �     �     �     �     �     �     �  ,   �  
   �  	   �  
   �                         %  $   1     V     n     �     �     �     �     �     �     �     �     �     	     	     	  �  )	     �
     �
  A   �
     3     <     O     U  "   ^     �  
   �  "   �     �  	   �     �     �     �     �                 	   "  	   ,  	   6     @  -   I  -   w  >   �     �     �     �     �          '     -     ;     C     L     Z  C   a  
   �  	   �  
   �     �     �     �     �     �  1   �     "     =     S     h     u     }     �  "   �     �  
   �     �  
   �     �                        #   (   2   ,      ?       '       1          :                                         5   -      +   9   	   <       "   *       )            %             $          /          &      .             ;   !                  8         6       7       4      =       
       0         >                      3    About Carburetor Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Client Transport Plugin Connecting… Czech DNS Port Disconnecting… Exit Country Exit Node Finland For Tractor France General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands Norway Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Save Bridges as a file in your local configs Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor is not running! Tractor is running Tractor is stopped Type of bridge Ukraine United Kingdom United States You have a new identity! _Check connection _Connect _Disconnect _New ID _Save translator-credits Project-Id-Version: French (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: French <https://hosted.weblate.org/projects/carburetor/translations/fr/>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 4.15-dev
 À propos de Carburetor Accepter des connexions Autoriser ou non les appareils extérieurs à utiliser ce réseau Autriche Auto (recommandé) Ponts Bulgarie Impossible d'enregistrer les ponts Canada Carburetor Extension de transport des clients Connexion… Tchéquie Port DNS Déconnexion… Pays de sortie Nœud de sortie Finlande Pour Tractor France Général Allemagne Port HTTP Irelande Port local via lequel Tractor serait écouté Port local via lequel Tractor serait écouté Port local pour un serveur DNS anonyme que vous pourriez avoir Moldavie Pays-Bas Norvège Veuillez vérifier leur syntaxe Pologne Ports Préférences Quitter Roumanie En exécution Russie Enregistrer les ponts dans un fichier de votre configuration locale Seychelles Singapour Port SOCKS Code source Espagne Arrêté Suède Suisse Le pays depuis lequel vous voulez être connecté Tractor est à l'arrêt ! Tractor est en marche Tractor est arrêté Type de pont Ukraine Royaume-Uni États-Unis Vous avez une nouvelle identité ! _Vérifier la connexion _Connecter _Déconnecter _Nouvel ID _Enregistrer – 