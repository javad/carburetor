��    H      \  a   �            !     2  *   E     p     x     �     �     �     �  
   �     �     �  $   �                    +  	   8     B     J     V     ]     v     ~  	   �     �  +   �  1   �  ;   �     2     :     F     K  
   R  "   ]     �     �     �     �     �     �     �  ,   �  
   �  	   �  
   �     	     	     	     	     #	  $   /	     T	     m	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     ,
     5
     A
     I
     O
     g
  �  z
          *  3   C  	   w     �  
   �     �     �     �  
   �     �     �  (        -     4     L     d     v  
   �     �     �     �     �     �     �     �  1   �  6   )  =   `     �     �     �     �  
   �  3   �     �       	     	   "     ,     4     B  ;   H  
   �     �     �     �     �  
   �     �  	   �  (   �          '     :     R     h     {     �     �     �     �     �     �     �     �               &      .     O         7   6       1      F          9       2   =   D   C   *              .                            A      ,             :               -      %                  )   #   @                  8   +   <      
   0             4   5      &   B   /          ?   >                 G   "   (      !   	      E          '   3   ;   $                         H    About Carburetor Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Client Transport Plugin Connecting… Copyright © 2019-2022, Tractor Team Czech DNS Port Disconnecting… Exit Country Exit Node Finland For Tractor France GTK frontend for Tractor General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands None Norway Obfuscated Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Save Bridges as a file in your local configs Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Type Type of bridge Ukraine United Kingdom United States Vanilla You have a new identity! _Check connection _Connect _Disconnect _New ID _Save _Toggle proxy on system translator-credits Project-Id-Version: Turkish (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Turkish <https://hosted.weblate.org/projects/carburetor/translations/tr/>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.15-dev
 Carburetor Hakkında Bağlantıları Kabul Et Harici aygıtların bu ağı kullanmasına izin ver Avusturya Otomatik (En iyisi) Köprüler Bulgaristan Köprüler kaydedilemiyor Kanada Carburetor İstemci Taşıma Eklentisi Bağlanıyor… Telif Hakkı © 2019-2022, Tractor Ekibi Çekya DNS Bağlantı Noktası Bağlantı kesiliyor… Çıkış Ülkesi Çıkış Düğümü Finlandiya Tractor için Fransa Tractor için GTK önyüzü Genel Almanya HTTP Bağlantı Noktası İrlanda Tractor'un dinleyeceği yerel bağlantı noktası HTTP tünelinin dinleyeceği yerel bağlantı noktası Anonim bir ad sunucunuzun olacağı yerel bağlantı noktası Moldova Hollanda Yok Norveç Gizlenmiş Lütfen köprülerin söz dizimini gözden geçirin Polonya Bağlantı Noktaları Tercihler Çıkış Romanya Çalışıyor Rusya Köprüleri yerel bir yapılandırma dosyası olarak kaydet Seyşeller Singapur Socks Bağlantı Noktası Kaynak Kodları İspanya Durduruldu İsveç İsviçre Üzerinden bağlanmak istediğiniz ülke Tractor bağlanamadı Tractor bağlandı Tractor çalışmıyor! Tractor çalışıyor Tractor durduruldu Tür Köprü türü Ukrayna Birleşik Krallık Amerika Birleşik Devletleri Sade Yeni bir kimliğiniz var! Bağlantıyı _denetle _Bağlan Bağlantıyı _kes _Yeni kimlik _Kaydet Sistemde vekil sunucu _aç/kapat Oğuz Ersen <oguz@ersen.moe> 