��    6      �  I   |      �     �  *   �     �     �     �     �            
   $     /     =     C     L  	   ]     g     o     v     ~  	   �     �  +   �  1   �  ;   �     2     :     F  "   M     p     w     }     �     �     �     �  
   �  	   �  
   �     �     �     �     �     �  $   �          /     B     U     ]     l     z     �     �     �  �  �     l	  9   }	     �	     �	     �	  	   �	  "   �	     	
  
   
     
     *
     ;
     H
     Y
  
   h
     s
     {
     �
     �
     �
  /   �
  1   �
  =     	   @     J     Y  )   a     �     �     �     �     �     �     �  	   �  	   �     �     �     �                          8     X     v     �     �     �     �     �     �     �               ,   $                 +   /      "      6   	                &          3   0      '                        1                 4             !   *             .          5             
   (      %      )           -   #                    2            Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Connecting… Czech DNS Port Disconnecting… Exit Node Finland France General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands Norway Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor is not running! Tractor is running Tractor is stopped Ukraine United Kingdom United States You have a new identity! _New ID _Save translator-credits Project-Id-Version: Portuguese (Brazil) (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Portuguese (Brazil) <https://hosted.weblate.org/projects/carburetor/translations/pt_BR/>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 4.15-dev
 Aceitar conexão Permitir ou não que dispositivos externos usem esta rede Áustria Automático (Melhor) Pontes Bulgária Não é possível salvar as pontes Canadá Carburetor Conectando… República Checa Porta de DNS Desconectando… Nodo de saída Finlândia França Geral Alemanha Porta de HTTP Irlanda Porta local na qual o Tractor estaria escutando Porta local na qual um túnel HTTP seria escutado Porta local na qual você teria um servidor de nomes anônimo Moldávia Países Baixos Noruega Por favor, verifique a sintaxe das pontes Polônia Portas Preferências Sair Romênia Rodando Rússia Seicheles Singapura Porta do Socket Código-fonte Espanha Parou Suécia Suíça O país do qual quer se conectar Tractor não está funcionando! Tractor está sendo executado Tractor está parado Ucrânia Reino Unido Estados Unidos da América Tem uma nova identidade! _Nova identidade (ID) _Salvar Créditos da tradução 