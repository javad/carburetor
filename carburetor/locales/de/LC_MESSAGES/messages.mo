��    @        Y         �     �     �  *   �     �     �     �     �     �       
              8     F     L     U     f  	   s     }     �     �     �     �     �  	   �     �  +   �  1   �  ;   1     m     u     �  "   �     �     �     �     �     �     �     �  ,   �  
     	     
   "     -     9     ?     G     N  $   Z          �     �     �     �     �     �     �     
	     	     %	     1	     9	     ?	  �  R	     �
     �
  @        V     b     x  	   �  ,   �     �  
   �     �     �  
   �  	   
          1     >     J     S  
   `     k  	   �     �  
   �     �  +   �  :   �  4        I     P     \      e     �     �     �     �  	   �     �     �  @   �  
             "  	   .     8  
   @     K     T  9   \     �     �     �     �     �     �       ,   &     S  
   g     r     {  
   �     �                  $   )   3   -      @       (       2          ;                                          6   .      ,   :   	   =       #   +       *            &             %          0          '      /             <   "                  9         7       8       5      >       
       1         ?      !               4    About Carburetor Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Client Transport Plugin Connecting… Czech DNS Port Disconnecting… Exit Country Exit Node Finland For Tractor France GTK frontend for Tractor General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands Norway Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Save Bridges as a file in your local configs Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor is not running! Tractor is running Tractor is stopped Type of bridge Ukraine United Kingdom United States You have a new identity! _Check connection _Connect _Disconnect _New ID _Save translator-credits Project-Id-Version: German (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: German <https://hosted.weblate.org/projects/carburetor/translations/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.15-dev
 Über Carburetor Verbindungen Akzeptieren Ermöglichen Sie externen Geräten, dieses Netzwerk zu verwenden Österreich Automatisch (Optimal) Brücken Bulgarien Die Bridges konnten nicht gespeichert werden Kanada Carburetor Klient Transport Plugin Verbindung wird erstellt … Tschechien DNS-Hafen Verbindung wird getrennt … Ausgang Land Exit-Knoten Finnland Für Tractor Frankreich GTK Frontend für Tractor Allgemein Deutschland HTTP-Hafen Irland Lokaler Port, auf dem Tractor lauschen soll Lokaler Port, an dem ein HTTP-Tunnel abgehört werden soll Lokaler Hafen, auf dem der anonyme DNS-Server läuft Moldau Niederlande Norwegen Bitte Syntax der Bridges prüfen Polen Häfen Einstellungen Beenden Rumänien Wird ausgeführt Russland Speichern Sie Bridges als Datei in Ihren lokalen Konfigurationen Seychellen Singapur Socks-Hafen Quellcode Spanien Angehalten Schweden Schweiz Das Land, aus dem Sie eine Verbindung herstellen möchten Tractor ist ausgeschaltet! Tractor wird ausgeführt Tractor wurde angehalten Art der Brücke Ukraine Vereinigtes Königreich Vereinigte Staaten Ihnen wurde eine neue Identität zugeordnet! _Verbindung prüfen _Verbinden _Trennen _Neue ID _Speichern Übersetzer Anerkennungen 