��    @        Y         �     �     �  *   �     �     �     �     �     �       
              8     F     L     U     f  	   s     }     �     �     �     �     �  	   �     �  +   �  1   �  ;   1     m     u     �  "   �     �     �     �     �     �     �     �  ,   �  
     	     
   "     -     9     ?     G     N  $   Z          �     �     �     �     �     �     �     
	     	     %	     1	     9	     ?	  �  R	     �
       9        P     Y     n  	   u  #        �  
   �     �     �     �     �     �            
   -     8     G     O     k     q     z     �  /   �  2   �  ,   �  	         *     9  )   A     k     t     {     �     �     �     �  ?   �  	   �  	   �                    %     ,     4     <  !   W     y     �     �     �     �     �     �       	        !     .     D  ^   M                  $   )   3   -      @       (       2          ;                                          6   .      ,   :   	   =       #   +       *            &             %          0          '      /             <   "                  9         7       8       5      >       
       1         ?      !               4    About Carburetor Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Client Transport Plugin Connecting… Czech DNS Port Disconnecting… Exit Country Exit Node Finland For Tractor France GTK frontend for Tractor General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands Norway Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Save Bridges as a file in your local configs Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor is not running! Tractor is running Tractor is stopped Type of bridge Ukraine United Kingdom United States You have a new identity! _Check connection _Connect _Disconnect _New ID _Save translator-credits Project-Id-Version: Portuguese (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Portuguese <https://hosted.weblate.org/projects/carburetor/translations/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 4.15-dev
 Sobre o Carburetor Aceitar conexão Permitir ou não que os aparelhos externos usem esta rede Áustria Automático (melhor) Pontes Bulgária Não é possível guardar as pontes Canadá Carburetor Plugin de cliente Transport A conectar… República Checa Porta de DNS A desconectar… País da saída Nó de saída Finlândia Para o Tractor França GTK frontend para o Tractor Geral Alemanha Porta de HTTP Irlanda Porta local na qual o Tractor estará à escuta Porta local a qual o túnel HTTP estará à escuta Porta local do servidor de nome anónimo DNS Moldávia Países Baixos Noruega Por favor, verifique a sintaxe das pontes Polónia Portas Preferências Sair Roménia A ser executado Rússia Guardar pontes como um ficheiro nas suas configurações locais Seicheles Singapura Porta socks Código-fonte Espanha Parado Suécia Suíça O país de onde quer ligar Tractor não está em execução! Tractor está em execução Tractor está parado Tipo de ponte Ucrânia Reino Unido Estados Unidos da América Tem uma nova identidade! _Verifique a conexão _Conectar _Desconectar _Nova identidade (ID) _Guardar Manuela Silva <mmsrs@sky.com>
Sérgio Morais <lalocas@protonmail.com>
Ssantos <ssantos@web.de> 