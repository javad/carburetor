��    @        Y         �     �     �  *   �     �     �     �     �     �       
              8     F     L     U     f  	   s     }     �     �     �     �     �  	   �     �  +   �  1   �  ;   1     m     u     �  "   �     �     �     �     �     �     �     �  ,   �  
     	     
   "     -     9     ?     G     N  $   Z          �     �     �     �     �     �     �     
	     	     %	     1	     9	     ?	  �  R	     9     K  0   Y     �     �     �     �     �     �  
   �     �     	          !     4     C     Q     ^  
   e  	   p     z     �  	   �     �     �  4   �  7   �  D   )  	   n  
   x  	   �     �     �     �     �     �  	   �  	   �     �  6   �     &     /     8     M     Y  	   e     o  
   x  "   �     �     �     �     �     �     �          $     :     I  
   R     ]     f     n                  $   )   3   -      @       (       2          ;                                          6   .      ,   :   	   =       #   +       *            &             %          0          '      /             <   "                  9         7       8       5      >       
       1         ?      !               4    About Carburetor Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Client Transport Plugin Connecting… Czech DNS Port Disconnecting… Exit Country Exit Node Finland For Tractor France GTK frontend for Tractor General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands Norway Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Save Bridges as a file in your local configs Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor is not running! Tractor is running Tractor is stopped Type of bridge Ukraine United Kingdom United States You have a new identity! _Check connection _Connect _Disconnect _New ID _Save translator-credits Project-Id-Version: Croatian (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Croatian <https://hosted.weblate.org/projects/carburetor/translations/hr/>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Weblate 4.15-dev
 Carburetor podaci Prihvati veze Dopusti vanjskim uređajima koristiti ovu mrežu Austrija Automatski (najbolje) Mostovi Bugarska Nije moguće spremiti mostove Kanada Carburetor Dodatak klijentskog transporta Povezivanje … Češka Priključak za DNS Odspajanje … Zemlja izlaza Čvor izlaza Finska Za Tractor Francuska GTK sučelje za Tractor Opće Njemačka Priključak za HTTP Irska Lokalni priključak na kojem se Tractor prisluškuje Lokalni priključak na kojem se HTTP tunel prisluškuje Lokalni priključak na kojem se može nalaziti anonimni poslužitelj Moldavija Nizozemska Norveška Provjeri sintaksu mostova Poljska Priključci Postavke Zatvori program Rumunjska Pokrenuto Rusija Spremi mostove kao datoteku u lokalnim konfiguracijama Sejšeli Singapur Priključak za Socks Izvorni kod Španjolska Prekinuto Švedska Švicarska Zemlja iz koje se želiš povezati Tractor nije pokrenut! Tractor je pokrenut Tractor je prekinut Vrsta mosta Ukrajina Ujedinjeno Kraljevstvo Sjedinjene Države Imaš novi identitet! _Provjeri vezu _Poveži _Odspojeno _Novi ID _Spremi Milo Ivir <mail@milotype.de> 