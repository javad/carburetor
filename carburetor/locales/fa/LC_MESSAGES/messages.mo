��    H      \  a   �            !     2  *   E     p     x     �     �     �     �  
   �     �     �  $   �                    +  	   8     B     J     V     ]     v     ~  	   �     �  +   �  1   �  ;   �     2     :     F     K  
   R  "   ]     �     �     �     �     �     �     �  ,   �  
   �  	   �  
   �     	     	     	     	     #	  $   /	     T	     m	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     ,
     5
     A
     I
     O
     g
  �  z
  !        2  ]   Q  
   �     �     �     �  4   �     *     7  1   J     |  >   �     �     �     �          $     6     C     [  *   h  
   �  
   �     �     �  6   �  :   �  6   8     o     ~     �     �     �  :   �     �     �               $     1  
   F  d   Q     �     �     �     �     �     �       
     =      +   ^     �  -   �  '   �  )   �     '     0     @     O     `     m  "   z     �     �     �     �     �  7   �  1   +         7   6       1      F          9       2   =   D   C   *              .                            A      ,             :               -      %                  )   #   @                  8   +   <      
   0             4   5      &   B   /          ?   >                 G   "   (      !   	      E          '   3   ;   $                         H    About Carburetor Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Client Transport Plugin Connecting… Copyright © 2019-2022, Tractor Team Czech DNS Port Disconnecting… Exit Country Exit Node Finland For Tractor France GTK frontend for Tractor General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands None Norway Obfuscated Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Save Bridges as a file in your local configs Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Type Type of bridge Ukraine United Kingdom United States Vanilla You have a new identity! _Check connection _Connect _Disconnect _New ID _Save _Toggle proxy on system translator-credits Project-Id-Version: Persian (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Persian <https://hosted.weblate.org/projects/carburetor/translations/fa/>
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 4.15-dev
 دربارهٔ کاربراتور پذیرش اتّصال‌ها اجازه به افزاره‌های خارجی برای استفاده از این شبکه اتریش خودکار (بهترین) پل‌ها بلغارستان نمی‌توان پل‌ها را ذخیره کرد کانادا کاربراتور افزایهٔ جابه‌جایی کارخواه در حال وصل شدن… حق رونوشت © ۱۳۹۸-۱۴۰۱، تیم تراکتور چک درگاه ساناد در حال قطع شدن… کشور خروجی گره خروجی فنلاند برای تراکتور فرانسه پیشانهٔ GTK برای تراکتور عمومی آلمان درگاه HTTP ایرلند درگاه محلّی برای شنود تراکتور درگاه محلّی برای شنود یک تونل HTTP درگاه محلّی برای ساناد ناشناس مولداوی هلند هیچ‌کدام نروژ مبهم شده لطفاً ترکیب پل‌ها را بررسی کنید لهستان درگاه‌ها ترجیحات خروج رومانی در حال اجرا روسیه ذخیرهٔ پل‌ها به شکل یک پرونده در پیکربندی‌های شخصیتان سیشل سنگاپور درگاه ساکس کد مبدأ اسپانیا متوقّف سوئد سوییس کشوری که می‌خواهید از آن وصل شوید تراکتور نتوانست وصل شود تراکتور وصل است تراکتور در حال اجرا نیست! تراکتور در حال اجراست تراکتور متوقّف شده است گونه گونهٔ پل اوکراین بریتانیا آمریکا وانیلی یک هویت جدید دارید! _بررسی اتّصال _وصل شدن _قطع شدن _هویت جدید _ذخیره _تغییر وضعیت پیشکار روی سامانه دانیال بهزادی <dani.behzi@ubuntu.com> 