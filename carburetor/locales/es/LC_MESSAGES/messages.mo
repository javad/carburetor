��    @        Y         �     �     �  *   �     �     �     �     �     �       
              8     F     L     U     f  	   s     }     �     �     �     �     �  	   �     �  +   �  1   �  ;   1     m     u     �  "   �     �     �     �     �     �     �     �  ,   �  
     	     
   "     -     9     ?     G     N  $   Z          �     �     �     �     �     �     �     
	     	     %	     1	     9	     ?	  �  R	     �
       H        ]     e     |     �      �     �  
   �     �     �     �     �                 	   .     8     E     M     e     m     v     �  2   �  7   �  @   �     9     B     P  $   X     }     �     �     �     �     �     �  =   �  
   �                    -     5     <     C  (   I     r     �     �     �     �     �     �     �       
   !     ,     9     J     S                  $   )   3   -      @       (       2          ;                                          6   .      ,   :   	   =       #   +       *            &             %          0          '      /             <   "                  9         7       8       5      >       
       1         ?      !               4    About Carburetor Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Client Transport Plugin Connecting… Czech DNS Port Disconnecting… Exit Country Exit Node Finland For Tractor France GTK frontend for Tractor General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands Norway Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Save Bridges as a file in your local configs Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor is not running! Tractor is running Tractor is stopped Type of bridge Ukraine United Kingdom United States You have a new identity! _Check connection _Connect _Disconnect _New ID _Save translator-credits Project-Id-Version: Spanish (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Spanish <https://hosted.weblate.org/projects/carburetor/translations/es/>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.15-dev
 Acerca de Carburetor Aceptar conexión Indica si debe permitirse a los dispositivos externos el uso de esta red Austria Automático (el mejor) Puentes Bulgaria No se pueden guardar los puentes Canadá Carburetor Plugin de transporte Cliente Conectando… Chequia Puerto de DNS Desconectando… Salir País Nodo de salida Finlandia Para Tractor Francia Interfaz GTK de Tractor General Alemania Puerto de HTTP Irlanda Puerto local en el que Tractor estaría escuchando El puerto local en el que se escucharía un túnel HTTP Puerto local en el cual tendrías un servidor de nombre anónimo Moldavia Países Bajos Noruega Compruebe la sintaxis de los puentes Polonia Puertos Preferencias Salir Rumania Ejecutándose Rusia Guarde Bridges como un archivo en sus configuraciones locales Seychelles Singapur Puerto de SOCKS Código Fuente España Parado Suecia Suiza El país desde el cual quiere conectarse No se está ejecutando Tractor! Tractor está ejecutándose Tractor se detuvo Tipo de puente Ucrania Reino Unido Estados Unidos Tiene una identidad nueva! Verificar conexión _Connectar _Desconectar _Identidad nueva _Guardar reconocimiento-traductor 